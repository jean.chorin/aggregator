# Aggregator

## Installation

Use a python *virtualenv* with Python version 3.4+. Install `paho-mqtt` in it using:

```bash
pip install paho-mqtt==1.5.0
```


## Starting up

To start the server on port 80, start it **as root** using:

```bash
python server.py
```

If you do not want to use root, change the `PORT_NUMBER` value in the `server.py` to `8080` for instance.


## Fetch data

To fetch the values of the metadata, use:

```bash
curl http://localhost/meta
```

With another port, this would be:

```bash
curl http://localhost:<PORT_NUMBER>/meta
```


To fetch the values of the topic `iot-sensor/sw/vibration` for the sensor `sensor-tv` in the machine `pump-1`, between the timestamp `0` and `1600000000`, use the following:

```bash
curl "http://localhost/iot?machine_id=pump-1&sensor_id=sensor-tv&topic=iot-sensor/sw/vibration&start_time=0&end_time=1681073033"
```


## Possible access points:

 * Accessible machines: `pump-1`
 * Accessible sensors: `sensor-tv`
 * Acessible topics: `iot-sensor/sw/vibration` and `iot-sensor/sw/temperature`
