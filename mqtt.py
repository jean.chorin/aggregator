import paho.mqtt.client as mqtt
import time
import logging
import json
from functools import partial

import db

logging.basicConfig(level=logging.DEBUG)
logger1 = logging.getLogger()

hostname = "broker-amq-mqtt-all-0-svc-rte-iotdemo.apps.ocp4.keithtenzer.com"


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("iot-sensor/sw/temperature", 0)
    client.subscribe("iot-sensor/sw/vibration", 0)


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))


# The callback for when a PUBLISH message is received from the server.
def on_message(persist, client, userdata, msg):
    #print("message: topic=" + msg.topic+" payload="+str(msg.payload))
    persist(msg.topic, msg.payload.decode())

class MqttReader(object):

    def __init__(self, database):
        self.database = database

        self.client = mqtt.Client(transport="websockets", client_id="XXX", clean_session=True)
        #self.client.enable_logger(logger1)
        #client.username_pw_set(username="iotuser", password="iotuser")
        self.client.on_connect = on_connect
        self.client.on_message = partial(on_message, self.persist)
        self.client.on_subscribe = on_subscribe


    def connect(self):
        self.client.connect_async(hostname, 80, 60)

        # Blocking call that processes network traffic, dispatches callbacks and
        # handles reconnecting.
        # Other loop*() functions are available that give a threaded interface and a
        # manual interface.
        self.client.loop_start()


    def persist(self, topic, payload):
        # Takes the MQTT message and put it into the database
        machine_id, sensor_id, value, timestamp = payload.split(",")
        self.database.put(machine_id, sensor_id, topic, timestamp, value)

