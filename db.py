import sqlite3
# https://docs.python.org/2/library/sqlite3.html
# https://docs.google.com/document/d/1xPZM1l0mCzRJiP2t2uLdqVMX8Zsxl8hgTrf3q7NcIlA/edit#

class Database(object):

    def __init__(self):
        self.conn = None

    def connect(self):
        self.conn = sqlite3.connect('gaia.db')
        self.cursor = self.conn.cursor()

        self.cursor.execute('''CREATE TABLE header (id text, machine_id text, sensor_id text, topic text)''')
        self.cursor.execute('''CREATE TABLE line (id text, id_header text, timestamp text, value text)''')


    def put(self, machine_id, sensor_id, topic, timestamp, value):
        pass

    def get(self, machine_id, sensor_id, topic, start_time, end_time):
        pass

    def close(self):
        self.conn.close()

class SimpleDatabase(object):

    def __init__(self):
        self.database = {}

    def connect(self):
        pass

    def put(self, machine_id, sensor_id, topic, timestamp, value):
        #print(f"Put in database: machine_id: {machine_id}, sensor_id: "
        #    f"{sensor_id}, topic: {topic}, timestamp: {timestamp}, value: {value}"
        #)
        key = (machine_id, sensor_id, topic)

        if key not in self.database:
            self.database[key] = []

        value = (timestamp, value)
        self.database[key] += [value]
        #print(self.database)

    def get(self, machine_id, sensor_id, topic, start_time, end_time):
        print(f"Get in machine_id: {machine_id}, sensor_id: {sensor_id}, topic: {topic} "
            f"from {start_time} to {end_time}."
        )

        key = (machine_id, sensor_id, topic)
        values_array = self.database.get(key)

        if values_array is None:
            raise ValueError()

        result = []
        for timestamp, value in values_array:
            if timestamp < start_time:
                continue

            if timestamp > end_time:
                break

            entry = {"timestamp": timestamp, "value": value}
            result.append(entry)

        return result


    def close(self):
        pass