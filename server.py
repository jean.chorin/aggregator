import json
import time
from functools import partial
from urllib.parse import urlparse

import db
from http.server import SimpleHTTPRequestHandler, HTTPServer
from mqtt import MqttReader

PORT_NUMBER = 80

database = db.SimpleDatabase()
database.connect()

METADATA_FILE ="metadata.json"
metadata = {}


def read_metadata(path):
    with open(path, "r") as f:
        raw_json = f.read()

    return json.loads(raw_json)


class MyHandler(SimpleHTTPRequestHandler):

    def __init__(self, *args, database=None, **kwargs):
        super().__init__(*args, **kwargs)


    def create_response(self, machine_id, sensor_id, topic, start_time, end_time):
        metadata = {
            "machine_id": machine_id,
            "sensor_id": sensor_id,
            "topic": topic,
            "start_track_time": start_time,
            "end_track_time": end_time,
            "received_time": str(int(time.time()))
        }
        values = database.get(machine_id, sensor_id, topic, start_time, end_time)
        answer = {"identification": metadata, "list_values": values}
        return answer

    def answer_mqtt(self):
        query = urlparse(self.path).query
        query_components = dict(qc.split("=") for qc in query.split("&"))
        if len(query_components) != 5:
            self.send_error(404)
            return


        try:
            response = self.create_response(**query_components)
        except ValueError:
            self.send_error(404)
            return

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        # Send the html message
        self.wfile.write(json.dumps(response).encode())
        return

    def answer_metadata(self):
        response = metadata

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        # Send the html message
        self.wfile.write(json.dumps(response).encode())
        return

    def do_GET(self):
        if "&" in self.path and self.path.startswith("/iot"):
            self.answer_mqtt()
        elif self.path == "/meta":
            self.answer_metadata()
        else:
            self.send_error(404)


try:
    metadata = read_metadata(METADATA_FILE)

    #Create a web server and define the handler to manage the incoming request
    server = HTTPServer(('', PORT_NUMBER), MyHandler)
    print('Started httpserver on port ' , PORT_NUMBER)

    #Wait forever for incoming http requests
    reader = MqttReader(database)
    reader.connect()
    server.serve_forever()

except KeyboardInterrupt:
    print('^C received, shutting down the web server')
    server.socket.close()
    database.close()
